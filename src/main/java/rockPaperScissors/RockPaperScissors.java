package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Random rand = new Random();
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");


    public void tie(){
        System.out.println("It's a tie!");
    }
    public void humanWin(){
        System.out.println("Human wins!");
        humanScore += 1;
    }
    public void computerWin(){
        System.out.println("Computer wins!");
        computerScore += 1;
    }

    public String humanDec(){
        while(true){       
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String choice = sc.next();
            for(String x : rpsChoices){
                if(x.equals(choice.toLowerCase())){
                    return choice;
                }
            }
            System.out.println("I do not understand " + choice +  " Could you try again?");
        }
        
    }
    
    public void run() {

        
        
        while(true){

            System.out.println("Let's play round " + roundCounter);

            //Computer
            int rNum = rand.nextInt(3);
            String comC = rpsChoices.get(rNum);

            // Human
            String choice = humanDec();
            
            System.out.print("Human chose " + choice + ", computer chose " + comC + ". ");
            if (choice.equals(comC.toLowerCase())){
                tie();
            }

            // Rock choice
            else if(choice.equals(rpsChoices.get(0))){
                if (comC.equals("paper")){
                    computerWin();
                }
                else if(comC.equals("scissors")){
                    humanWin();
                }
            }

            // Paper choice
            else if(choice.equals(rpsChoices.get(1))){
                if (comC.equals("scissors")){
                    computerWin();
                }
                else if(comC.equals("rock")){
                    humanWin();
                }
            }
            else if(choice.equals(rpsChoices.get(2))){
                if (comC.equals("rock")){
                    computerWin();
                }
                else if(comC.equals("paper")){
                    humanWin();
                }
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            System.out.println("Do you wish to continue playing? (y/n)?");
            String con = sc.next();
            if(con.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            else if(con.equals("y")){
                roundCounter += 1;
                continue;
            }
        }
    }

    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
